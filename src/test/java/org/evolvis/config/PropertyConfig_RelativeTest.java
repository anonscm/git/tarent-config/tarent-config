package org.evolvis.config;


import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import static org.evolvis.config.TUtils.*;

/**
 * This TestSuite tests the PropertyConfig based imlpementation with
 * an absolute configuration path.
 *
 * @author Tino Rink <t.rink@tarent.de>
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
		ConfigurationT.class
})
public class PropertyConfig_RelativeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		makeSureSystemPropertiesAreNotSet();

		// Set necessary system properties
		System.setProperty(Constants.CONFIGURATION_IMPL, PropertyConfig.class.getName());
		System.setProperty(Constants.TARENT_CONFIG_KEY, "target/test-classes/tarent.properties");

		createTestTarentPropertiesFile();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		makeSureSystemPropertiesAreNotSet();
		deleteFile(System.getProperty("user.dir") + "/target/test-classes/tarent.properties");
	}
}
