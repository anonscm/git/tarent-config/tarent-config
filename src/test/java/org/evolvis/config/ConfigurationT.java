package org.evolvis.config;

import org.junit.Test;

import static org.evolvis.config.TUtils.assertContainsSameProperties;
import static org.junit.Assert.*;

/**
 * This test class contains all unit tests that must perform equally regardless
 * a concrete implementation. only different system properties are allowed for setup the
 * concrete implementation and are done inside the corresponding TestSuite.
 *
 * @author Tino Rink <t.rink@tarent.de>
 * @author Christian Cikryt <c.cikryt@tarent.de>
 */
public class ConfigurationT {

	private final Configuration config = ConfigFactory.getConfiguration();

	@Test
	public void testGetString() {
		assertEquals("givenname", config.get("tarent.test.attr.name"));
		assertNull(config.get("tarent.sso.attr.nonexistant"));
	}

	@Test
	public void testGetStringWithDefaults() {
		assertEquals("givenname", config.get("tarent.test.attr.name", "default"));
		assertEquals("default", config.get("tarent.sso.attr.nonexistant", "default"));
	}

	@Test
	public void testGetAsBooleanString() {
		assertTrue(config.toString(), config.getAsBoolean("tarent.test.attr.true"));
		assertFalse(config.toString(), config.getAsBoolean("tarent.test.attr.false"));
		assertFalse(config.toString(), config.getAsBoolean("tarent.test.attr.nonexistant"));
	}

	@Test
	public void testGetAsBooleanWithDefault() {
		assertTrue(config.toString(), config.getAsBoolean("tarent.test.attr.true", false));
		assertFalse(config.toString(), config.getAsBoolean("tarent.test.attr.false", true));
		assertTrue(config.toString(), config.getAsBoolean("tarent.test.attr.nonexistant", true));
	}

	@Test
	public void testGetBooleanWithoutValue() {
		assertFalse(config.toString(), config.getAsBoolean("tarent.test.attr.booleanempty"));
		assertFalse(config.toString(), config.getAsBoolean("tarent.test.attr.booleanempty", true));
	}

	@Test
	public void testGetAsLong() {
		assertEquals((Long) 123456789L, config.getAsLong("tarent.test.attr.long"));
	}

	@Test(expected = NumberFormatException.class)
	public void testGetAsLongWithInvalidKey() {
		config.getAsLong("tarent.test.attr.nonexistant");
	}

	@Test
	public void testGetAsLongWithDefault() {
		assertEquals((Long) 123456789L, config.getAsLong("tarent.test.attr.long", 54321));
		assertEquals((Long) 54321L, config.getAsLong("tarent.test.attr.nonexistant", 54321));
	}

	@Test
	public void testGetLongWithoutValue() {
		assertNull(config.toString(), config.getAsLong("tarent.test.attr.longempty"));
		assertNull(config.toString(), config.getAsLong("tarent.test.attr.longempty", 0));
	}

	@Test
	public void testGetAsDouble() {
		assertEquals(1234.5678, config.getAsDouble("tarent.test.attr.double"), 0);
	}

	@Test(expected = NullPointerException.class)
	public void testGetAsDoubleWithInvalidKey() {
		config.getAsDouble("tarent.test.attr.nonexistant");
	}

	@Test
	public void testGetAsDoubleWithDefault() {
		assertEquals(1234.5678, config.getAsDouble("tarent.test.attr.double"), 0);
		assertEquals(8765.4321, config.getAsDouble("tarent.test.attr.nonexistant", 8765.4321d), 0);
	}

	@Test
	public void testGetDoubleWithoutValue() {
		assertNull(config.toString(), config.getAsDouble("tarent.test.attr.doubleempty"));
		assertNull(config.toString(), config.getAsDouble("tarent.test.attr.doubleempty", 0));
	}

	@Test
	public void testGetSubConfiguration() {
		final Configuration subConf = config.getSubConfiguration("user");
		assertNotNull(subConf);
		assertNotSame("config and subConfig must not be the same", config, subConf);
		assertEquals(subConf, config.getSubConfiguration("user"));
		assertTrue(subConf == config.getSubConfiguration("user"));
		assertEquals("overwritten", subConf.get("tarent.test.attr.status"));
	}

	@Test
	public void subConfCantBeLoaded() {
		final Configuration subConf = config.getSubConfiguration("unknown");
		assertNotNull(subConf);
		assertContainsSameProperties(config, subConf);
		assertNotSame(config, subConf);
	}

	@Test
	public void testSetPrefix() {
		config.setPrefix("something");
		assertEquals("true", config.get("tarent.prefixtest.attr.true"));
		assertEquals("false", config.get("tarent.prefixtest.attr.false"));
		assertEquals(null, config.get("tarent.prefixtest.attr.nonexistant"));
		assertEquals("default", config.get("tarent.prefixtest.attr.nonexistant", "default"));
	}
}
