package org.evolvis.config;

import org.junit.Assert;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Properties;

import static junit.framework.Assert.assertEquals;

/**
 * @author Christian Cikryt <c.cikryt@tarent.de>
 */
public class TUtils {
	static void assertFileContainsProperties(String filename, String[]... properties) throws IOException {
		Properties p = new Properties();
		p.load(new FileReader(new File(filename)));
		assertEquals(properties.length, p.size());
		for (String[] property : properties)
			assertEquals(property[1], p.getProperty(property[0]));
	}

	static void assertContainsSameProperties(Configuration config, Configuration subConf) {
		Properties properties = getPropertiesFieldOfConfigurationObject(config);
		Properties subProperties = getPropertiesFieldOfConfigurationObject(subConf);
		Assert.assertEquals(properties.stringPropertyNames().size(), subProperties.stringPropertyNames().size());
		for (String propertyName : properties.stringPropertyNames()) {
			Assert.assertEquals(properties.getProperty(propertyName), subProperties.getProperty(propertyName));
		}
	}

	static Properties getPropertiesFieldOfConfigurationObject(Configuration config) {
		final Field propertiesField;
		try {
			propertiesField = config.getClass().getDeclaredField("properties");
			propertiesField.setAccessible(true);
			return (Properties) propertiesField.get(config);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	static void createTestTarentPropertiesFile() throws IOException {
		Properties p = new Properties();
		p.setProperty("tarent.test.attr.name", "givenname");
		p.setProperty("tarent.test.attr.status", "inetuserstatus");
		p.setProperty("tarent.test.attr.long", "123456789");
		p.setProperty("tarent.test.attr.longempty", "");
		p.setProperty("tarent.test.attr.double", "1234.5678");
		p.setProperty("tarent.test.attr.doubleempty", "");
		p.setProperty("tarent.test.attr.true", "true");
		p.setProperty("tarent.test.attr.false", "false");
		p.setProperty("tarent.test.attr.booleanempty", "");
		p.setProperty("tarent.test.attr.nan", "notanumber");
		p.setProperty("tarent-config.propertyconfig.subconfpattern", "${IDENTIFIER}.properties");
		p.setProperty("something.tarent.prefixtest.attr.true", "true");
		p.setProperty("something.tarent.prefixtest.attr.false", "false");
		p.setProperty("internal.testkey", "this will not be seen");
		p.setProperty("external.testkey", "default-external");
		p.setProperty("prefixed.external.testkey", "prefixed-default-external");
		p.store(new FileOutputStream(System.getProperty("user.dir") + "/target/test-classes/tarent.properties"), null);
	}

	static void makeSureSystemPropertiesAreNotSet() {
		System.getProperties().remove(Constants.CONFIGURATION_IMPL);
		System.getProperties().remove(Constants.TARENT_CONFIG_KEY);
	}

	static void deleteFile(String path) {
		new File(path).delete();
	}

	static void createFileWithProperties(String path, String[]... properties) throws IOException {
		Properties p = new Properties();
		for (String[] property : properties) {
			p.setProperty(property[0], property[1]);
		}
		p.store(new FileOutputStream(path), null);
	}
}
