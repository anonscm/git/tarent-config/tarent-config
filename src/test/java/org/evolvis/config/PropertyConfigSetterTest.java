package org.evolvis.config;

import org.junit.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import static org.evolvis.config.TUtils.*;

/**
 * @author Christian Cikryt <c.cikryt@tarent.de>
 */
public class PropertyConfigSetterTest {

	private static final String tarentConfigPath = System.getProperty("user.dir") + "/target/test-classes/tarent-writetest.properties";

	@BeforeClass
	public static void setUpBeforeClass() {
		makeSureSystemPropertiesAreNotSet();

		System.setProperty(Constants.CONFIGURATION_IMPL, PropertyConfig.class.getName());
		System.setProperty(Constants.TARENT_CONFIG_KEY, tarentConfigPath);
	}

	@Before
	public void setUp() throws IOException {
		deleteTestFiles();
		createTestFiles();
	}

	@Test
	public void changeKnownProperty() throws IOException {
		Configuration config = ConfigFactory.getConfiguration();
		config.set("tarent.test.attr.first", "overwritten");

		assertFileContainsProperties(tarentConfigPath, new String[]{"tarent.test.attr.first", "overwritten"});
	}

	@Test
	public void changeKnownPropertyTheGroupIdIsIgnored() throws IOException {
		Configuration config = ConfigFactory.getConfiguration();
		config.set("arbitrary group id", "tarent.test.attr.first", "overwritten");

		assertFileContainsProperties(tarentConfigPath, new String[]{"tarent.test.attr.first", "overwritten"});
	}

	@Test
	public void setNewProperty() throws IOException {
		Configuration config = ConfigFactory.getConfiguration();
		config.set("arbitrary group id", "tarent.test.attr.new", "newvalue");

		assertFileContainsProperties(tarentConfigPath, new String[]{"tarent.test.attr.new", "newvalue"}, new String[]{"tarent.test.attr.first", "firstvalue"});
	}

	@Test(expected = ConfigException.class)
	public void tryToChangePropertyWithoutWritePermission() throws IOException {
		new File(tarentConfigPath).setWritable(false);
		Configuration config = ConfigFactory.getConfiguration();
		config.set("arbitrary group id", "tarent.test.attr.first", "overwritten");
	}

	@After
	public void tearDown() {
		deleteTestFiles();
	}

	@AfterClass
	public static void tearDownAfterClass() {
		makeSureSystemPropertiesAreNotSet();
	}

	private void createTestFiles() throws IOException {
		Properties p = new Properties();
		p.setProperty("tarent.test.attr.first", "firstvalue");
		p.store(new FileWriter(new File(tarentConfigPath)), null);
	}

	private void deleteTestFiles() {
		deleteFile(tarentConfigPath);
	}
}
