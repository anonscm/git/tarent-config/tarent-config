package org.evolvis.config;


import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import static org.evolvis.config.TUtils.makeSureSystemPropertiesAreNotSet;

/**
 * This TestSuite tests the PropertyConfig based imlpementation with
 * an absolute configuration path.
 *
 * @author Tino Rink <t.rink@tarent.de>
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
		ConfigurationT.class
})
public class PropertyFolderConfig_RelativeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		makeSureSystemPropertiesAreNotSet();

		// Set necessary system properties
		System.setProperty(Constants.CONFIGURATION_IMPL, PropertyFolderConfig.class.getName());
		System.setProperty(Constants.TARENT_CONFIG_KEY, "target/test-classes/testPropertyFolder");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		makeSureSystemPropertiesAreNotSet();
	}
}
