package org.evolvis.config;

import org.junit.*;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import static junit.framework.Assert.*;
import static org.evolvis.config.TUtils.*;

/**
 * @author Christian Cikryt <c.cikryt@tarent.de>
 */
public class PropertyConfigSubConfigSetterTest {

	private static final String parentConfig = System.getProperty("user.dir") + "/target/test-classes/tarent-writetest.properties";
	private final String subConfigPath = System.getProperty("user.dir") + "/target/test-classes/subconfig.properties";

	@BeforeClass
	public static void setUpBeforeClass() {
		makeSureSystemPropertiesAreNotSet();

		System.setProperty(Constants.CONFIGURATION_IMPL, PropertyConfig.class.getName());
		System.setProperty(Constants.TARENT_CONFIG_KEY, parentConfig);
	}

	@Before
	public void setUp() throws IOException {
		deleteTestFiles();
		createTestFiles();
	}

	@Test
	public void writeParentPropertyInSubconfig() throws IOException {
		Configuration config = ConfigFactory.getConfiguration();
		Configuration subConfig = config.getSubConfiguration("subconfig");

		assertNotSame(config, subConfig);

		subConfig.set("tarent.test.attr.first", "overwritten");
		assertFileContainsProperties(parentConfig, new String[]{"tarent.test.attr.first", "firstvalue"}, new String[]{"tarent-config.propertyconfig.subconfpattern", "${IDENTIFIER}.properties"});
		assertFileContainsProperties(subConfigPath, new String[]{"tarent.test.attr.first", "overwritten"}, new String[]{"tarent.test.attr.second", "secondvalue"});
	}

	@Test
	public void UnchangedPropertiesMustNotBeWrittenIntoSubconfig() throws IOException {
		Configuration config = ConfigFactory.getConfiguration();
		Configuration subConfig = config.getSubConfiguration("subconfig");
		subConfig.set("tarent.test.attr.first", "overwritten");

		Properties p = new Properties();
		p.load(new FileReader(new File(subConfigPath)));
		assertNull(p.getProperty("tarent-config.propertyconfig.subconfpattern"));
	}

	@After
	public void tearDown() {
		deleteTestFiles();
	}

	@AfterClass
	public static void tearDownAfterClass() {
		makeSureSystemPropertiesAreNotSet();
	}

	private void createTestFiles() throws IOException {
		Properties p = new Properties();
		p.setProperty("tarent.test.attr.first", "firstvalue");
		p.setProperty("tarent-config.propertyconfig.subconfpattern", "${IDENTIFIER}.properties");
		p.store(new FileWriter(new File(parentConfig)), null);
		p = new Properties();
		p.setProperty("tarent.test.attr.second", "secondvalue");
		p.store(new FileWriter(new File(subConfigPath)), null);
	}

	private void deleteTestFiles() {
		deleteFile(parentConfig);
		deleteFile(subConfigPath);
	}
}
