package org.evolvis.config;

import org.junit.*;

import java.io.IOException;

import static junit.framework.Assert.assertEquals;
import static org.evolvis.config.TUtils.*;

/**
 * @author Christian Cikryt <c.cikryt@tarent.de>
 */
public class ConfigurationLoaderTest {

	private final static String TEST_CONFIG_FILE_PATH = System.getProperty("user.dir") + "/target/test-classes/tarent-config";

	@BeforeClass
	public static void setUpBeforeClass() throws IOException {
		deleteFile(TEST_CONFIG_FILE_PATH);
		createFileWithProperties(TEST_CONFIG_FILE_PATH, new String[]{Constants.CONFIGURATION_IMPL, "chosen.impl"}, new String[]{Constants.TARENT_CONFIG_KEY, "file"});
	}

	@Before
	public void setUp() throws Exception {
		makeSureSystemPropertiesAreNotSet();
	}

	@Test
	public void noSystemPropertiesreadConfigfile() {
		ConfigurationLoader l = new ConfigurationLoader(TEST_CONFIG_FILE_PATH);
		l.loadSystemPropertiesFromConfigfile();
		assertEquals("chosen.impl", System.getProperty(Constants.CONFIGURATION_IMPL));
		assertEquals("file", System.getProperty(Constants.TARENT_CONFIG_KEY));
	}

	@Test
	public void systemPropertiesAreNotOverwritten() {
		System.setProperty(Constants.CONFIGURATION_IMPL, "value");
		System.setProperty(Constants.TARENT_CONFIG_KEY, "value");
		ConfigurationLoader l = new ConfigurationLoader(TEST_CONFIG_FILE_PATH);
		l.loadSystemPropertiesFromConfigfile();
		assertEquals("value", System.getProperty(Constants.CONFIGURATION_IMPL));
		assertEquals("value", System.getProperty(Constants.TARENT_CONFIG_KEY));

	}

	@After
	public void tearDown() throws Exception {
		makeSureSystemPropertiesAreNotSet();
	}

	@AfterClass
	public static void tearDownAfterClass() {
		deleteFile(TEST_CONFIG_FILE_PATH);
	}
}
