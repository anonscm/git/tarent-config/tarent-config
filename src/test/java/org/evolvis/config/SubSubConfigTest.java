package org.evolvis.config;

import org.junit.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import static junit.framework.Assert.assertEquals;
import static org.evolvis.config.TUtils.*;

/**
 * @author Christian Cikryt <c.cikryt@tarent.de>
 */
public class SubSubConfigTest {

	private static final String parentConfig = System.getProperty("user.dir") + "/target/test-classes/tarent-writetest.properties";
	private final String subConfigPath = System.getProperty("user.dir") + "/target/test-classes/subconfig.properties";
	private final String subSubConfigPath = System.getProperty("user.dir") + "/target/test-classes/subsubconfig.properties";

	@BeforeClass
	public static void setUpBeforeClass() {
		makeSureSystemPropertiesAreNotSet();

		System.setProperty(Constants.CONFIGURATION_IMPL, PropertyConfig.class.getName());
		System.setProperty(Constants.TARENT_CONFIG_KEY, parentConfig);
	}

	@Before
	public void setUp() throws IOException {
		deleteTestFiles();
		createTestFiles();
	}

	@Test
	public void getSubSubProperties() {
		Configuration config = ConfigFactory.getConfiguration();
		Configuration subConfig = config.getSubConfiguration("subconfig");
		Configuration subSubConfig = subConfig.getSubConfiguration("subsubconfig");
		assertEquals("firstvalue", subSubConfig.get("tarent.test.attr.first"));
		assertEquals(Constants.SUB_CONFIGURATION_IDENTIFIER_MARKER + ".properties", subSubConfig.get(Constants.SUB_CONFIGURATION_PATTERN_KEY));
		assertEquals("secondvalue", subSubConfig.get("tarent.test.attr.second"));
	}

	@Test
	public void overwriteProperties() throws IOException {
		Configuration config = ConfigFactory.getConfiguration();
		Configuration subConfig = config.getSubConfiguration("subconfig");
		Configuration subSubConfig = subConfig.getSubConfiguration("subsubconfig");
		subSubConfig.set("tarent.test.attr.first", "overwritten");

		assertFileContainsProperties(parentConfig, new String[]{"tarent.test.attr.first", "firstvalue"}, new String[]{Constants.SUB_CONFIGURATION_PATTERN_KEY, Constants.SUB_CONFIGURATION_IDENTIFIER_MARKER + ".properties"});
		assertFileContainsProperties(subConfigPath, new String[]{"tarent.test.attr.second", "secondvalue"});
		assertFileContainsProperties(subSubConfigPath, new String[]{"tarent.test.attr.first", "overwritten"});
	}

	@After
	public void tearDown() {
		deleteTestFiles();
	}

	@AfterClass
	public static void tearDownAfterClass() {
		makeSureSystemPropertiesAreNotSet();
	}

	private void createTestFiles() throws IOException {
		Properties p = new Properties();
		p.setProperty("tarent.test.attr.first", "firstvalue");
		p.setProperty(Constants.SUB_CONFIGURATION_PATTERN_KEY, Constants.SUB_CONFIGURATION_IDENTIFIER_MARKER + ".properties");
		p.store(new FileWriter(new File(parentConfig)), null);
		p = new Properties();
		p.setProperty("tarent.test.attr.second", "secondvalue");
		p.store(new FileWriter(new File(subConfigPath)), null);
	}

	private void deleteTestFiles() {
		deleteFile(parentConfig);
		deleteFile(subConfigPath);
		deleteFile(subSubConfigPath);
	}
}
