package org.evolvis.config;

import org.junit.*;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import static junit.framework.Assert.*;
import static org.evolvis.config.TUtils.*;

/**
 * @author Christian Cikryt <c.cikryt@tarent.de>
 */
public class PropertyConfigFolderSubConfigSetterTest {

	private static final String parentConfigFolder = System.getProperty("user.dir") + "/target/test-classes/testWriteFolder/";
	private final String parentConfig1 = parentConfigFolder + "1.properties";
	private final String parentConfig2 = parentConfigFolder + "2.properties";

	private static final String subConfigFolder = System.getProperty("user.dir") + "/target/test-classes/subConfigFolder/";
	private final String subConfig1 = subConfigFolder + "1.properties";
	private final String subConfig2 = subConfigFolder + "2.properties";

	@BeforeClass
	public static void setUpBeforeClass() {
		makeSureSystemPropertiesAreNotSet();

		System.setProperty(Constants.CONFIGURATION_IMPL, PropertyFolderConfig.class.getName());
		System.setProperty(Constants.TARENT_CONFIG_KEY, parentConfigFolder);
	}

	@Before
	public void setUp() throws IOException {
		deleteTestFilesAndFolders();
		createTestFilesAndFolders();
	}

	@Test
	public void getSubConfiguration() {
		Configuration config = ConfigFactory.getConfiguration();
		Configuration subConfig = config.getSubConfiguration("subConfigFolder");

		assertEquals("overwritten", subConfig.get("tarent.test.attr.first"));
		assertEquals("secondvalue", subConfig.get("tarent.test.attr.second"));
		assertEquals("thirdvalue", subConfig.get("tarent.test.attr.third"));
	}

	@Test
	public void overwriteParentPropertyInSubconfig() throws IOException {
		Configuration config = ConfigFactory.getConfiguration();
		Configuration subConfig = config.getSubConfiguration("subConfigFolder");
		subConfig.set("tarent.test.attr.second", "overwritten");
		subConfig.set("tarent.test.attr.third", "overwritten");
		assertFileContainsProperties(subConfig1, new String[]{"tarent.test.attr.first", "overwritten"}, new String[]{"tarent.test.attr.second", "overwritten"});
		assertFileContainsProperties(subConfig2, new String[]{"tarent.test.attr.third", "overwritten"});
		assertFileContainsProperties(parentConfig1, new String[]{"tarent.test.attr.first", "firstvalue"}, new String[]{"tarent.test.attr.second", "secondvalue"});
		assertFileContainsProperties(parentConfig2, new String[]{"tarent.test.attr.third", "thirdvalue"}, new String[]{Constants.SUB_CONFIGURATION_PATTERN_KEY, "../" + Constants.SUB_CONFIGURATION_IDENTIFIER_MARKER});
	}

	@Test
	public void UnchangedPropertiesMustNotBeWrittenIntoSubconfig() throws IOException {
		Configuration config = ConfigFactory.getConfiguration();
		Configuration subConfig = config.getSubConfiguration("subConfigFolder");
		subConfig.set("tarent.test.attr.third", "overwritten");

		Properties p = new Properties();
		p.load(new FileReader(new File(subConfig2)));
		assertNull(p.getProperty("tarent-config.propertyconfig.subconfpattern"));
		assertEquals("overwritten", p.getProperty("tarent.test.attr.third"));
	}

	@After
	public void tearDown() {
		deleteTestFilesAndFolders();
	}

	private void deleteTestFilesAndFolders() {
		deleteFile(parentConfig1);
		deleteFile(parentConfig2);
		deleteFile(parentConfigFolder);
		deleteFile(subConfig1);
		deleteFile(subConfig2);
		deleteFile(subConfigFolder);
	}

	@AfterClass
	public static void tearDownAfterClass() {
		makeSureSystemPropertiesAreNotSet();
	}

	private void createTestFilesAndFolders() throws IOException {
		new File(parentConfigFolder).mkdir();
		new File(subConfigFolder).mkdir();
		Properties p = new Properties();
		p.setProperty("tarent.test.attr.first", "firstvalue");
		p.setProperty("tarent.test.attr.second", "secondvalue");
		p.store(new FileWriter(new File(parentConfig1)), null);
		p = new Properties();
		p.setProperty("tarent.test.attr.third", "thirdvalue");
		p.setProperty(Constants.SUB_CONFIGURATION_PATTERN_KEY, "../" + Constants.SUB_CONFIGURATION_IDENTIFIER_MARKER);
		p.store(new FileWriter(new File(parentConfig2)), null);
		p = new Properties();
		p.setProperty("tarent.test.attr.first", "overwritten");
		p.store(new FileWriter(new File(subConfig1)), null);
	}
}
