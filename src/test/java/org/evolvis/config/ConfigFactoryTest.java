package org.evolvis.config;

import org.junit.*;

import static org.evolvis.config.TUtils.*;

/**
 * This tests the ConfigFactory default and explicit instrumented behavior.
 *
 * @author Tino Rink <t.rink@tarent.de>
 * @author Christian Cikryt <c.cikryt@tarent.de>
 */
public class ConfigFactoryTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		createTestTarentPropertiesFile();
	}

	@Before
	public void setUp() {
		makeSureSystemPropertiesAreNotSet();
	}

	@After
	public void tearDown() {
		makeSureSystemPropertiesAreNotSet();
	}

	@Test
	public void getDefaultConfiguration() {
		System.setProperty(Constants.TARENT_CONFIG_KEY, System.getProperty("user.dir") + "/target/test-classes/tarent.properties");
		final Configuration configuration = ConfigFactory.getConfiguration();
		Assert.assertNotNull("the default configuration must not be null", configuration);
		Assert.assertEquals(PropertyConfig.class.getName(), configuration.getClass().getName());
	}

	@Test
	public void getExplicitConfiguration() {
		System.setProperty(Constants.TARENT_CONFIG_KEY, System.getProperty("user.dir") + "/target/test-classes/tarent.properties");
		System.setProperty(Constants.CONFIGURATION_IMPL, PropertyConfig.class.getName());
		final Configuration configuration = ConfigFactory.getConfiguration();
		Assert.assertNotNull("a valid explicit configuration must not be null", configuration);
	}

	@Test
	public void getExplicitPropertyFolderConfiguration() {
		System.setProperty(Constants.TARENT_CONFIG_KEY, System.getProperty("user.dir") + "/target/test-classes/");
		System.setProperty(Constants.CONFIGURATION_IMPL, PropertyFolderConfig.class.getName());
		final Configuration configuration = ConfigFactory.getConfiguration();
		Assert.assertNotNull("a valid explicit configuration must not be null", configuration);
	}

	@Test(expected = ConfigException.class)
	public void getInvalidConfiguration() {
		System.setProperty(Constants.TARENT_CONFIG_KEY, System.getProperty("user.dir") + "/target/test-classes/tarent.properties");
		System.setProperty(Constants.CONFIGURATION_IMPL, "a.invalid.config.Implementation");
		ConfigFactory.getConfiguration();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		deleteFile(System.getProperty("user.dir") + "/target/test-classes/tarent.properties");
	}

}
