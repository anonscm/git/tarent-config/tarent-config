package org.evolvis.config;

import org.junit.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import static org.evolvis.config.TUtils.*;

/**
 * @author Christian Cikryt <c.cikryt@tarent.de>
 */
public class PropertyFolderConfigSetterTest {

	private static final String tarentConfigFolder = System.getProperty("user.dir") + "/target/test-classes/testWriteFolder/";
	private final String folderConfig1 = tarentConfigFolder + "1.properties";
	private final String folderConfig2 = tarentConfigFolder + "2.properties";

	@BeforeClass
	public static void setUpBeforeClass() {
		makeSureSystemPropertiesAreNotSet();

		System.setProperty(Constants.CONFIGURATION_IMPL, PropertyFolderConfig.class.getName());
		System.setProperty(Constants.TARENT_CONFIG_KEY, tarentConfigFolder);
	}

	@Before
	public void setUp() throws IOException {
		deleteTestFilesAndFolders();
		createTestFilesAndFolders();
	}

	@Test
	public void changeKnownProperties() throws IOException {
		Configuration config = ConfigFactory.getConfiguration();
		config.set("tarent.test.attr.first", "overwritten");
		config.set("tarent.test.attr.third", "overwritten");

		assertFileContainsProperties(folderConfig1, new String[]{"tarent.test.attr.first", "overwritten"}, new String[]{"tarent.test.attr.second", "secondvalue"});
		assertFileContainsProperties(folderConfig2, new String[]{"tarent.test.attr.third", "overwritten"});
	}

	@Test(expected = ConfigException.class)
	public void tryToChangePropertyWithoutWritePermission() throws IOException {
		new File(folderConfig1).setWritable(false);
		Configuration config = ConfigFactory.getConfiguration();
		config.set("tarent.test.attr.first", "overwritten");
	}

	@Test
	public void setNewProperty() throws IOException {
		Configuration config = ConfigFactory.getConfiguration();
		config.set("1.properties", "tarent.test.attr.new", "newvalue");

		assertFileContainsProperties(folderConfig1, new String[]{"tarent.test.attr.first", "firstvalue"}, new String[]{"tarent.test.attr.second", "secondvalue"}, new String[]{"tarent.test.attr.new", "newvalue"});
	}

	@Test
	public void setNewPropertyInNewFile() throws IOException {
		String newConfigFile = tarentConfigFolder + "3.properties";
		new File(tarentConfigFolder).setWritable(true);
		Configuration config = ConfigFactory.getConfiguration();
		config.set("3.properties", "tarent.test.attr.new", "newvalue");

		assertFileContainsProperties(newConfigFile, new String[]{"tarent.test.attr.new", "newvalue"});
	}

	@Test
	public void ifThePropertyIsKnownAWrongGroupIdIsIgnored() throws IOException {
		Configuration config = ConfigFactory.getConfiguration();
		config.set("arbitrary id", "tarent.test.attr.first", "overwritten");
		assertFileContainsProperties(folderConfig1, new String[]{"tarent.test.attr.first", "overwritten"}, new String[]{"tarent.test.attr.second", "secondvalue"});
	}

	@Test(expected = ConfigException.class)
	public void tryToSetKnownPropertyWithoutGroupId() {
		Configuration config = ConfigFactory.getConfiguration();
		config.set("tarent.test.attr.new", "newvalue");
	}

	@After
	public void tearDown() {
		deleteTestFilesAndFolders();
	}

	@AfterClass
	public static void tearDownAfterClass() {
		makeSureSystemPropertiesAreNotSet();
	}

	private void deleteTestFilesAndFolders() {
		deleteFile(folderConfig1);
		deleteFile(folderConfig2);
		deleteFile(tarentConfigFolder + "3.properties");
		deleteFile(tarentConfigFolder);
	}

	private void createTestFilesAndFolders() throws IOException {
		new File(tarentConfigFolder).mkdir();
		Properties p = new Properties();
		p.setProperty("tarent.test.attr.first", "firstvalue");
		p.setProperty("tarent.test.attr.second", "secondvalue");
		p.store(new FileWriter(new File(folderConfig1)), null);
		p = new Properties();
		p.setProperty("tarent.test.attr.third", "thirdvalue");
		p.store(new FileWriter(new File(folderConfig2)), null);
	}
}
