package org.evolvis.config;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.evolvis.config.TUtils.makeSureSystemPropertiesAreNotSet;

/**
 * @author Christian Cikryt <c.cikryt@tarent.de>
 */
public class PropertyConfigCreationTest {
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		makeSureSystemPropertiesAreNotSet();

		// Set necessary system properties
		System.setProperty(Constants.CONFIGURATION_IMPL, PropertyConfig.class.getName());
	}

	@Test
	public void configFileDoesNotExistButCanBeCreated() {
		System.setProperty(Constants.TARENT_CONFIG_KEY, System.getProperty("user.dir") + "/target/test-classes/not-existent.properties");
		ConfigFactory.getConfiguration();
	}

	@Test(expected = ConfigException.class)
	public void configFileNeitherExistsNorCanBeCreated() {
		System.setProperty(Constants.TARENT_CONFIG_KEY, System.getProperty("user.dir") + "/target/test-classes/unwritable/not-existent.properties");
		ConfigFactory.getConfiguration();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		makeSureSystemPropertiesAreNotSet();
	}
}
