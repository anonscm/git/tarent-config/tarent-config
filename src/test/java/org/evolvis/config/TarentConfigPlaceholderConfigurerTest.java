package org.evolvis.config;

import org.evolvis.config.spring.TarentConfigPlaceholderConfigurer;
import org.junit.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import static org.evolvis.config.Constants.*;
import static org.evolvis.config.TUtils.*;

/**
 * @author Robert Schuster
 * @author Christian Cikryt <c.cikryt@tarent.de>
 */
public class TarentConfigPlaceholderConfigurerTest {

	private Properties props;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		createTestTarentPropertiesFile();
	}

	@Before
	public void setUp() {
		// Makes sure the property is not set by a previous test.
		System.getProperties().remove(CONFIGURATION_IMPL);
		System.getProperties().remove(TARENT_CONFIG_KEY);

		props = new Properties();
		try {
			props.load(new ClassPathResource("/internal.properties").getInputStream());
		} catch (IOException e) {
			throw new IllegalStateException();
		}
	}

	@After
	public void tearDown() {
		System.getProperties().remove(TARENT_CONFIG_KEY);
		System.getProperties().remove(CONFIGURATION_IMPL);
	}

	@Test
	public void testDefaultExternal() {

		TarentConfigPlaceholderConfigurer configurer = new TarentConfigPlaceholderConfigurer();

		// Accesses a key whose value is supposed to be in the 'internal'
		// property file.
		Assert.assertEquals("internal", configurer.resolvePlaceholder(
				"internal.testkey", props));

		// Accesses a key whose value does not exist in the 'internal'
		// property file and is instead resolved from the default external
		// one (which is found in classpath:/tarent.properties).
		Assert.assertEquals("default-external", configurer.resolvePlaceholder(
				"external.testkey", props));

		configurer.setExternalPrefix("prefixed");
		// Accesses a key whose value does not exist in the 'internal'
		// property file and is instead resolved from the default external
		// one (which is found in classpath:/tarent.properties) by using a
		// key that was preprended with the external prefix.
		Assert.assertEquals("prefixed-default-external", configurer.resolvePlaceholder(
				"external.testkey", props));
	}

	@Test
	public void testExplicitExternal() {

		// set explicit tarent.config
		System.setProperty(TARENT_CONFIG_KEY, "target/test-classes/external.properties");

		TarentConfigPlaceholderConfigurer configurer = new TarentConfigPlaceholderConfigurer();

		// Accesses a key whose value is supposed to be in the 'internal'
		// property file.
		Assert.assertEquals("internal", configurer.resolvePlaceholder("internal.testkey", props));

		// Accesses a key whose value does not exist in the 'internal'
		// property file and is instead resolved from the explicitly given
		// external one (which in this case is 'classpath:/external.properties').
		Assert.assertEquals("explicit-external", configurer.resolvePlaceholder("external.testkey", props));

		configurer.setExternalPrefix("prefixed");
		// Accesses a key whose value does not exist in the 'internal'
		// property file and is instead resolved from the default external
		// one (which is found in classpath:/tarent.properties) by using a
		// key that was preprended with the external prefix.
		Assert.assertEquals("prefixed-explicit-external", configurer.resolvePlaceholder(
				"external.testkey", props));
	}

	@Test
	public void testSpringPlaceholderResolution() {

		final ApplicationContext ac = new ClassPathXmlApplicationContext("/testAppContext.xml");
		Assert.assertNotNull("ApplicationContext must not be null", ac);

		final Map<?, ?> testMap = (Map<?, ?>) ac.getBean("testMap");
		Assert.assertNotNull("testMap must exist", testMap);

		Assert.assertEquals("123456789", testMap.get("testKey"));
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		deleteFile(System.getProperty("user.dir") + "/target/test-classes/tarent.properties");
	}
}
