package org.evolvis.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author Christian Cikryt <c.cikryt@tarent.de>
 */
class ConfigurationLoader {

	private static Log log = LogFactory.getLog(ConfigurationLoader.class);

	private final File configuration;

	ConfigurationLoader(String configFilename) {
		this.configuration = new File(configFilename);
	}

	void loadSystemPropertiesFromConfigfile() {
		Properties p = new Properties();
		if (configuration.isFile()) {
			try {
				p.load(new FileInputStream(configuration));
			} catch (IOException e) {
				log.warn("Initializing: error reading configuration file '" + Constants.CONFIGURATION_FILE_PATH + "'.");
			}
		} else {
			log.info("Initializing: configuration file '" + Constants.CONFIGURATION_FILE_PATH + "' does not exist");
		}

		for (String key : p.stringPropertyNames()) {
			if (System.getProperty(key) == null) {
				System.setProperty(key, p.getProperty(key));
			}
		}
	}
}
