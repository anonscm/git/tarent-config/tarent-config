package org.evolvis.config.spring;

import java.util.Properties;

import org.evolvis.config.ConfigFactory;
import org.evolvis.config.Configuration;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**
 * An implementation that uses Spring's built-in {@link PropertyPlaceholderConfigurer}
 * class and extends it to use an {@link Configuration} Object to resolve Placeholder
 * configured external via tarent-config.
 *
 * <p>See {@link #resolvePlaceholder(String, Properties)} for details.</p>
 *
 * <p>See {@link ConfigFactory} for details of the {@link Configuration} Object creation.</p>
 *
 * @author Robert Schuster <r.schuster@tarent.de>
 * @author Tino Rink <t.rink@tarent.de>
 */
public class TarentConfigPlaceholderConfigurer extends PropertyPlaceholderConfigurer {

	private Configuration configuration = ConfigFactory.getConfiguration();
	
	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}
	
	/**
	 * Sets a prefix which is prepended to the key when a value is looked up
	 * in the external configuration (tarent-config).
	 *  
	 * <p>The actual separator string between the prefix and the remaining
	 * key can be set via the {@link #setPrefixSeparator(String)} method.</p>
	 * 
	 * <p>By default no prefix is set. This leads to nothing being prepended
	 * by default.</p>
	 * 
	 * @param externalPrefix
	 */
	public void setExternalPrefix(String externalPrefix) {
		configuration.setPrefix(externalPrefix);
	}

	/**
	 * Sets the string that is put between the external prefix and a given
	 * key when lookup up externally.
	 * 
	 * <p>The default value is <code>.</code>(dot).
	 * 
	 * @param prefixSeparator
	 */
	public void setPrefixSeparator(String prefixSeparator) {
		configuration.setPrefixSeparator(prefixSeparator);
	}

	/**
	 * First looks up the requested value in the built-in {@link Property} object
	 * and <em>only</em> if that resolves to a <code>null</code> value a look-up
	 * into the (external) {@link Configuration} object is performed.
	 * 
	 * <p>Be aware that set an external prefix by {@link #setExternalPrefix(String)},
	 * a configuration key like 'foo.value' and a prefix like 'bar' will result into 
	 * an internal lookup for 'foo.value' and an external lookup for 'bar.foo.value'.</p>
	 */
	@Override
	public String resolvePlaceholder(String placeholder, Properties props) {

		final String builtInValue = super.resolvePlaceholder(placeholder, props);

		if (builtInValue == null && configuration != null) {
			return configuration.get(placeholder);
		}

		return builtInValue;
	}
}
