package org.evolvis.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import static org.evolvis.config.Constants.EMPTY_STRING;

/**
 * <p>An {@link Configuration} implementation that used a folder of property files for loading the
 * config-keys and -values. The key-value-pairs are stored inside a {@link java.util.Properties}
 * object. The property files inside the defined folder will be loaded in lexicographically order
 * based on {@link File#compareTo(File)}.</p>
 * <p/>
 * <p>The relative or absolute property folder URI is defined by the SystemProperty 'tarent.config',
 * e.g.: '-Dtarent.config=/path/to/property/folder/'.</p>
 * <p/>
 * <p>This implementation supports {@link #getSubConfiguration(String)}.</p>
 * <p/>
 * <p>Use the SystemProperty {@link Constants#CONFIGURATION_IMPL} to activate this implementation:
 * '-Dtarent.config.impl=org.evolvis.config.PropertyFolderConfig'.</p>
 *
 * @author Tino Rink <t.rink@tarent.de>
 * @author Christian Cikryt <c.cikryt@tarent.de>
 */
public class PropertyFolderConfig extends AbstractPropertyConfig implements Configuration {

	private final Log log = LogFactory.getLog(getClass());

	private final Map<String, Configuration> subConfigs = new ConcurrentHashMap<String, Configuration>();

	private final String configFolder;

	private Map<String, String> propertyToConfigFileMapping = new HashMap<String, String>();

	private Map<String, Properties> filenameToContentMapping = new HashMap<String, Properties>();

	private final Properties properties;

	public PropertyFolderConfig() {
		properties = new Properties();
		configFolder = getBaseConfigPath();
		loadPropertyFolder(configFolder);
	}

	protected PropertyFolderConfig(Properties parentProperties, String subConfigFolder) {
		properties = new Properties(parentProperties);
		configFolder = subConfigFolder.endsWith("/") ? subConfigFolder : subConfigFolder + "/";
		loadPropertyFolder(subConfigFolder);
	}

	/* Folder is not created if it does not exists */
	private void loadPropertyFolder(String configFolder) {

		if (configFolder == null || configFolder.equals(EMPTY_STRING))
			throw new ConfigException("No config folder given.");

		final File propertyFolder = new File(configFolder);

		if (!propertyFolder.exists()) {
			throw new ConfigException("reading property config folder failed, given folder does not exist: " + configFolder);
		} else if (propertyFolder.isDirectory()) {

			final File[] propertyFiles = propertyFolder.listFiles();

			if (propertyFiles == null) {
				log.warn("couldn't open config folder: " + configFolder);
			} else if (propertyFiles.length == 0) {
				log.warn("no files found in config folder: " + configFolder);
			} else {

				Arrays.sort(propertyFiles);

				for (File configFile : propertyFiles) {
					try {
						loadPropertiesFromFile(configFile);
					} catch (IOException e) {
						log.warn("couldn't open property file: " + configFile + " in folder: " + configFolder + " because of: " + e.getMessage());
					}
				}
			}
		} else {
			throw new ConfigException("reading property config folder failed, given path isn't a folder: " + configFolder);
		}
	}

	private void loadPropertiesFromFile(File configFile) throws IOException {
		Properties tmpProperties = new Properties();
		tmpProperties.load(new FileReader(configFile));
		filenameToContentMapping.put(configFile.getName(), tmpProperties);
		for (String key : tmpProperties.stringPropertyNames()) {
			propertyToConfigFileMapping.put(key, configFile.getName());
			properties.setProperty(key, tmpProperties.getProperty(key));
		}
	}

	@Override
	public Properties getProperties() {
		return this.properties;
	}

	@Override
	public Configuration getSubConfiguration(String identifier) {

		if (!subConfigs.containsKey(identifier)) {
			final String subConfigPath = getSubConfigPath(identifier);
			if (subConfigPath != null && new File(subConfigPath).isDirectory()) {
				final PropertyFolderConfig subConfig = new PropertyFolderConfig(this.properties, subConfigPath);
				subConfig.propertyToConfigFileMapping.putAll(propertyToConfigFileMapping);
				subConfigs.put(identifier, subConfig);
			} else {
				log.error("getSubConfiguration(String) failed, subConfigPath couldn't be derived for identifier=" + identifier);
				subConfigs.put(identifier, new PropertyFolderConfig());
			}
		}
		return subConfigs.get(identifier);
	}

	@Override
	public void set(String key, String value) {
		String filename = propertyToConfigFileMapping.get(key);
		if (filename == null)
			throw new ConfigException("trying to set unknown property without giving a group id.");
		set(filename, key, value);
	}

	@Override
	public void set(String groupId, String key, String value) {
		String filename = propertyToConfigFileMapping.containsKey(key) ? propertyToConfigFileMapping.get(key) : groupId;
		propertyToConfigFileMapping.put(key, filename);
		Properties p = filenameToContentMapping.get(filename);
		if (p == null) {
			p = new Properties();
			filenameToContentMapping.put(filename, p);
		}
		properties.setProperty(key, value);
		p.setProperty(key, value);
		try {
			p.store(new FileWriter(new File(configFolder + filename)), null);
		} catch (IOException e) {
			throw new ConfigException(e);
		}
	}
}
