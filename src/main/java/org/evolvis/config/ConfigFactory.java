package org.evolvis.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * <p>The {@link #getConfiguration()} should be used to create / load a new Configuration Object.
 * The default Configuration class is {@link PropertyConfig}.</p>
 * <p/>
 * <p>Use the SystemProperty {@link Constants#CONFIGURATION_IMPL} to define a different implementation, e.g.:
 * '-Dtarent.config.impl=my.own.config.Implementation'.</p>
 *
 * @author Tino Rink <t.rink@tarent.de>
 * @author Christian Cikryt <c.cikryt@tarent.de>
 */
public final class ConfigFactory {

	private static Log log = LogFactory.getLog(ConfigFactory.class);

	private static ConfigurationLoader configLoader = new ConfigurationLoader(Constants.CONFIGURATION_FILE_PATH);

	private ConfigFactory() {
	}

	/**
	 * Load and return a Configuration (default or defined by the SystemProperty 'tarent.config.impl') object.
	 *
	 * @return a Configuration object
	 * @throws ConfigException if implementation couldn't be created or initialized.
	 */
	public static Configuration getConfiguration() {

		configLoader.loadSystemPropertiesFromConfigfile();

		final String configurationClass = System.getProperty(Constants.CONFIGURATION_IMPL, Constants.DEFAULT_CONFIGURATION_IMPL);

		try {
			if (log.isInfoEnabled()) {
				log.info("trying to create a new config instance of type: " + configurationClass);
			}
			return (Configuration) Class.forName(configurationClass).newInstance();
		} catch (Exception e) {
			log.error("loading configuration implementation failed for: " + configurationClass +
					" with error: " + e.getLocalizedMessage());
			throw new ConfigException("loading configuration implementation failed for type: " + configurationClass, e);
		}
	}

}
