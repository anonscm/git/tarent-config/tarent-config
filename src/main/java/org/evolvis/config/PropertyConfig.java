package org.evolvis.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import static org.evolvis.config.Constants.DEFAULT_TARENT_PROPERTIY_FILE;

/**
 * <p>A {@link Configuration} implementation that uses a {@link java.util.Properties} object and
 * a property file for loading the config-keys and -values.</p>
 * <p/>
 * <p>The relative or absolute URI of the property file is defined by the SystemProperty 'tarent.config',
 * e.g.: '-Dtarent.config=/path/to/file.properties'.</p>
 * <p/>
 * <p>If no SystemProperty is given, the resource '/tarent.properties' is tried to be loaded
 * from the classpath.</p>
 * <p/>
 * <p>Use the SystemProperty {@link Constants#CONFIGURATION_IMPL} to activate this implementation:
 * '-Dtarent.config.impl=org.evolvis.config.PropertyConfig'.</p>
 *
 * @author Tino Rink <t.rink@tarent.de>
 * @author Fabian Bieker (minor cc bugfix)
 * @author Florian Wallner
 * @author Christian Cikryt <c.cikryt@tarent.de>
 */
public class PropertyConfig extends AbstractPropertyConfig implements Configuration {

	private final Log log = LogFactory.getLog(getClass());

	private final Map<String, PropertyConfig> subConfigs = new ConcurrentHashMap<String, PropertyConfig>();

	private Properties properties = new Properties();

	/* maps to the path of the config file from where the properties have been loaded */
	private String configPath;

	public PropertyConfig() {

		final String tarentConfig = getBaseConfigPath();

		if (!succeededToLoadPropertiesFromFilesystem(tarentConfig)) {
			if (!succeededToLoadPropertiesFromClasspath(tarentConfig)) {
				// fallback
				log.warn("tarent.config couldn't be loaded, trying to load default properties from classpath: " + DEFAULT_TARENT_PROPERTIY_FILE);
				tryToLoadDefaultPropertiesFromClasspath();
			}
		}
	}

	protected PropertyConfig(Properties parentProperties, String subConfigPath, String prefix, String prefixSeparator) {
		properties = new Properties(parentProperties);
		configPath = subConfigPath;
		setPrefix(prefix);
		setPrefixSeparator(prefixSeparator);
	}

	private boolean succeededToLoadPropertiesFromFilesystem(String tarentConfig) {
		final File file = new File(tarentConfig);
		try {
			if (file.createNewFile())
				assert (file.canWrite());
			else
				properties.load(new FileInputStream(tarentConfig));
			configPath = tarentConfig;
			return true;
		} catch (IOException e) {
			log.warn("loading base config from filesystem failed for tarent.config=" + tarentConfig, e);
			return false;
		}
	}

	private boolean succeededToLoadPropertiesFromClasspath(String tarentConfig) {
		try {
			properties.load(PropertyConfig.class.getResourceAsStream(tarentConfig));
			configPath = PropertyConfig.class.getResource(tarentConfig).getPath();
			return true;
		} catch (IOException e) {
			log.error("loading base config from classpath failed for tarent.config=" + tarentConfig, e);
			return false;
		}
	}

	private void tryToLoadDefaultPropertiesFromClasspath() {
		try {
			properties.load(PropertyConfig.class.getResourceAsStream(DEFAULT_TARENT_PROPERTIY_FILE));
			configPath = PropertyConfig.class.getResource(DEFAULT_TARENT_PROPERTIY_FILE).getPath();
		} catch (IOException e) {
			log.error("loading default baseConfig from classpath: " + DEFAULT_TARENT_PROPERTIY_FILE + " failed", e);
			throw new ConfigException(e);
		}
	}

	@Override
	public Properties getProperties() {
		return this.properties;
	}

	@Override
	public Configuration getSubConfiguration(final String identifier) {

		if (!subConfigs.containsKey(identifier)) {

			final String subConfigPath = getSubConfigPath(identifier);
			PropertyConfig subConfig = new PropertyConfig(properties, subConfigPath, getPrefix(), getPrefixSeparator());

			if (subConfigPath != null) {
				try {
					PropertyConfig tmpConfig = new PropertyConfig(properties, subConfigPath, getPrefix(), getPrefixSeparator());
					tmpConfig.properties.load(new FileInputStream(new File(subConfigPath)));
					subConfig = tmpConfig;
				} catch (Exception e) {
					log.error("getSubConfiguration(String) failed with exception=" + e, e);
				}
			} else {
				log.error("getSubConfiguration(String) failed, subConfigPath couldn't be derivated for identifier=" + identifier);
			}
			subConfigs.put(identifier, subConfig);
		}
		return subConfigs.get(identifier);
	}

	@Override
	public void set(String key, String value) {
		properties.setProperty(key, value);
		try {
			properties.store(new FileWriter(new File(configPath)), null);
		} catch (IOException e) {
			throw new ConfigException(e);
		}
	}

	/**
	 * This implementation ignores the groupId as there is only one property file.
	 *
	 * @param groupId the name of the group that the property belongs to
	 * @param key	 the identifier of the property
	 * @param value   the new value of the property
	 */
	@Override
	public void set(String groupId, String key, String value) {
		set(key, value);
	}

	@Override
	public String toString() {
		return "PropertyConfig [properties=" + properties + "]";
	}
}
