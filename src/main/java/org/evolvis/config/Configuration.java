package org.evolvis.config;

/**
 * The interface that each implementation of Configuration must implements.
 *
 * @author Tino Rink <t.rink@tarent.de>
 * @author Robert Schuster <r.schuster@tarent.de>
 * @author Christian Cikryt <c.cikryt@tarent.de>
 */
public interface Configuration {

	/**
	 * The default key prefix each implementation of Configuration must be
	 * used as default value.
	 */
	String DEFAULT_PREFIX = "";

	/**
	 * The default prefix-key-separator each implementation of Configuration
	 * must be used as default value.
	 */
	String DEFAULT_PREFIX_SEPARATOR = ".";

	/**
	 * Get the configuration value for the given key or null if not found
	 *
	 * @param key The configuration key to get the value for
	 * @return The value for the given key.
	 */
	String get(final String key);

	/**
	 * Get the configuration value for the given key or the given defaultValue
	 *
	 * @param key		  The configuration key to get the value for
	 * @param defaultValue The default value to return if the given key is not found
	 * @return The value for the given key.
	 */
	String get(final String key, final String defaultValue);

	/**
	 * Get the configuration value for the given key as boolean.
	 *
	 * @param key The configuration key to get the value for
	 * @return The value for the given key.
	 */
	Boolean getAsBoolean(final String key);

	/**
	 * Get the configuration value for the given key as boolean.
	 *
	 * @param key		  The configuration key to get the value for
	 * @param defaultValue The default value to return if the given key is not found
	 * @return The value for the given key.
	 */
	Boolean getAsBoolean(final String key, final boolean defaultValue);

	/**
	 * Get the configuration value for the given key as <code>long</code> value.
	 *
	 * @param key The configuration key to get the value for
	 * @return The value for the given key as long.
	 */
	Long getAsLong(final String key);

	/**
	 * Get the configuration value for the given key as <code>long</code> value.
	 *
	 * @param key		  The configuration key to get the value for
	 * @param defaultValue The default value to return if the given key is not found
	 * @return The value for the given key as long.
	 */
	Long getAsLong(final String key, final long defaultValue);

	/**
	 * Get the configuration value for the given key as <code>double</code>
	 *
	 * @param key The configuration key to get the value for
	 * @return The value for the given key as double.
	 */
	Double getAsDouble(final String key);

	/**
	 * Get the configuration value for the given key as <code>double</code>
	 *
	 * @param key		  The configuration key to get the value for
	 * @param defaultValue The default value to return if the given key is not found
	 * @return The value for the given key as double.
	 */
	Double getAsDouble(final String key, final double defaultValue);

	/**
	 * Return a sub-configuration that is a derivation of the base configuration
	 * extends or overwritten with a new (sub)set of configuration data.
	 * <p/>
	 * <p>If a prefix ({@link #setPrefix(String)}) or prefix separator ({@link
	 * #setPrefixSeparator(String)}) is set, the sub configuration will take over
	 * it.</p>
	 *
	 * @param identifier of the sub-configuration, mustn't be null
	 * @return derivated sub-configuration for given identifier
	 */
	Configuration getSubConfiguration(final String identifier);

	/**
	 * Sets a prefix which is prepended automatically to the key when
	 * a value is looked up.
	 * <p/>
	 * <p>The actual separator string between the prefix and the remaining
	 * key can be set via the {@link #setPrefixSeparator(String)} method.</p>
	 * <p/>
	 * <p>By default {@link #DEFAULT_PREFIX} is set. This leads to nothing
	 * being prepended by default.</p>
	 *
	 * @param prefix the string prepended to the key
	 */
	void setPrefix(String prefix);

	/**
	 * Sets the string that is put between the prefix and a given
	 * key.
	 * <p/>
	 * <p>The default {@link #DEFAULT_PREFIX_SEPARATOR} is set.
	 *
	 * @param prefixSeparator the string to separate prefix and key
	 */
	void setPrefixSeparator(String prefixSeparator);

	/**
	 * Changes the value of a property identified by a key.
	 * As this variant of the set method does not provide a group identifier,
	 * the property must either be already known or there has to be only
	 * one possible location where to set it.
	 *
	 * @param key   the identifier of the property
	 * @param value the new value of the property
	 */
	void set(String key, String value);

	/**
	 * Sets the value of a property identified by a key.
	 * Each implementation can map the groupId to a storage location for the property
	 * according to its own rules.
	 *
	 * @param groupId the name of the group that the property belongs to
	 * @param key	 the identifier of the property
	 * @param value   the new value of the property
	 */
	void set(String groupId, String key, String value);
}
