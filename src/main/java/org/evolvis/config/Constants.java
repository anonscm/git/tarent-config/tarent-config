package org.evolvis.config;

/**
 * Hold global constants used in this module
 *
 * @author Tino Rink <t.rink@tarent.de>
 * @author Christian Cikryt <c.cikryt@tarent.de>
 */
public final class Constants {

	private Constants() {
	}

	public static final String CONFIGURATION_FILE_PATH = "/etc/tarent-config/factory.properties";

	public static final String CONFIGURATION_IMPL = "tarent.config.impl";

	public static final String TARENT_CONFIG_KEY = "tarent.config";

	public static final String DEFAULT_CONFIGURATION_IMPL = PropertyConfig.class.getName();

	public static final String DEFAULT_TARENT_PROPERTIY_FILE = "/tarent.properties";

	public static final String SUB_CONFIGURATION_PATTERN_KEY = "tarent-config.propertyconfig.subconfpattern";

	public static final String SUB_CONFIGURATION_IDENTIFIER_MARKER = "${IDENTIFIER}";

	public static final String EMPTY_STRING = "";
}
