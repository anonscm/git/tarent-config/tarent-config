package org.evolvis.config;

/**
 * 
 * @author Tino Rink <t.rink@tarent.de>
 */
public class ConfigException extends RuntimeException {

	private static final long serialVersionUID = -1424846409933591675L;

	public ConfigException(String errorMsg) {
		super(errorMsg);
	}

	public ConfigException(Exception exception) {
		super(exception);
	}

	public ConfigException(String errorMsg, Exception exception) {
		super(errorMsg, exception);
	}
}
