package org.evolvis.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.Properties;

import static org.evolvis.config.Constants.*;

public abstract class AbstractPropertyConfig implements Configuration {

	private final Log log = LogFactory.getLog(getClass());

	private String prefix = DEFAULT_PREFIX;

	private String prefixSeparator = DEFAULT_PREFIX_SEPARATOR;

	public abstract Properties getProperties();


	/**
	 * Get the string which is prepended automatically to the key when
	 * a value is looked up.
	 * <p/>
	 * <p>By default {@link #DEFAULT_PREFIX} is set. This leads to nothing
	 * being prepended by default.</p>
	 *
	 * @return the string prepended to the key
	 */
	public String getPrefix() {
		return this.prefix;
	}

	@Override
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	/**
	 * Get the string that is put between the prefix and a given key.
	 * <p/>
	 * <p>The default {@link #DEFAULT_PREFIX_SEPARATOR} is set.</p>
	 *
	 * @return the string that separates prefix and key
	 */
	public String getPrefixSeparator() {
		return this.prefixSeparator;
	}

	@Override
	public void setPrefixSeparator(String prefixSeparator) {
		this.prefixSeparator = prefixSeparator;
	}


	protected String getImpl(final String key, final String defaultValue) {
		// If prefix exists, then resolve with the prefix prepended.
		if (getPrefix().length() > 0) {
			return getProperties().getProperty(getPrefix() + getPrefixSeparator() + key, defaultValue);
		} else {
			return getProperties().getProperty(key, defaultValue);
		}
	}

	protected String getBaseConfigPath() {
		String tarentConfigPath = System.getProperty(TARENT_CONFIG_KEY);
		if (tarentConfigPath == null || tarentConfigPath.equals(EMPTY_STRING)) {
			log.info(TARENT_CONFIG_KEY + " was not defined, return default property file name: " + DEFAULT_TARENT_PROPERTIY_FILE);
			tarentConfigPath = DEFAULT_TARENT_PROPERTIY_FILE;
		}
		return tarentConfigPath;
	}

	protected String getSubConfigPath(final String identifier) {
		if (identifier == null || identifier.equals(EMPTY_STRING)) {
			log.error("getSubConfigPath(String) called with null or empty identifier");
			throw new IllegalArgumentException("getSubConfigPath(String) called with null or empty identifier");
		}

		final String subConfigPath;
		final String subConfigPattern = get(SUB_CONFIGURATION_PATTERN_KEY);

		if (subConfigPattern == null || subConfigPattern.equals(EMPTY_STRING)) {
			log.warn("getSubConfigPath(String) couldn't find SUB_CONFIGURATION_PATTERN=" + SUB_CONFIGURATION_PATTERN_KEY + " in base-config");
			subConfigPath = null;
		} else if (!subConfigPattern.contains(SUB_CONFIGURATION_IDENTIFIER_MARKER)) {
			log.warn("getSubConfigPath(String) couldn't find marker=" + SUB_CONFIGURATION_IDENTIFIER_MARKER + " in pattern=" + subConfigPattern);
			subConfigPath = null;
		} else {
			final File subConfigFile = new File(subConfigPattern.replace(SUB_CONFIGURATION_IDENTIFIER_MARKER, identifier));

			if (subConfigFile.isAbsolute())
				subConfigPath = subConfigFile.getPath();
			else
				subConfigPath = buildAbsolutePath(new File(getBaseConfigPath()), subConfigFile);
		}

		return subConfigPath;
	}

	protected String getImpl(final String key) {
		return getImpl(key, null);
	}

	@Override
	public String get(final String key) {
		return getImpl(key);
	}

	@Override
	public String get(final String key, final String defaultValue) {
		return getImpl(key, defaultValue);
	}

	@Override
	public Boolean getAsBoolean(final String key) {
		String val = getImpl(key);
		return Boolean.parseBoolean(val);
	}

	@Override
	public Boolean getAsBoolean(String key, boolean defaultValue) {
		String val = getImpl(key);
		return val != null ? Boolean.parseBoolean(val) : defaultValue;
	}

	@Override
	public Long getAsLong(final String key) {
		String val = getImpl(key);
		return "".equals(val) ? null : Long.parseLong(val);
	}

	@Override
	public Long getAsLong(String key, long defaultValue) {
		String val = getImpl(key);
		if (val == null)
			return defaultValue;
		else if (val.equals(""))
			return null;
		else
			return Long.parseLong(val);
	}

	@Override
	public Double getAsDouble(String key) {
		String val = getImpl(key);
		return "".equals(val) ? null : Double.parseDouble(val);
	}

	@Override
	public Double getAsDouble(String key, double defaultValue) {
		String val = getImpl(key);
		if (val == null)
			return defaultValue;
		else if (val.equals(""))
			return null;
		else
			return Double.parseDouble(val);
	}

	private String buildAbsolutePath(File tarentConfigFile, File subConfigFile) {
		if (tarentConfigFile.isDirectory())
			return tarentConfigFile.getPath() + File.separator + subConfigFile.getPath();
		else
			return tarentConfigFile.getParent() + File.separator + subConfigFile.getPath();
	}
}
